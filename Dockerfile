FROM denoland/deno:1.18.2
WORKDIR /app
USER deno
COPY programme.ts .
COPY roulette.ts .
CMD ["run", "programme.ts"]
