# Groupe

- Jérémie BELLANGER
- Geoffrey CARPENTIER
- Anthony GUEUSSET

# Utilisation

## Sans docker

### Installer deno

Installer Deno [deno](https://deno.land/manual/getting_started/installation):
```sh
curl -fsSL https://deno.land/x/install/install.sh | sh
```

### Lancer le programme

```
deno run programme.ts
```
## Avec docker

```
docker build -t allumettes-deno .
docker run allumettes-deno
```
