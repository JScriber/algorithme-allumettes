
# Allumer

Algorithme: **allumer**

**Entrée**:
```
g: ENTIER
d: ENTIER
p: REEL # Probabilité d'avoir droite. Ambidextre = 0.5. Un droitier tendra vers 1. Un gaucher tendra vers 0.
```

**Résultat**: ENTIER

**Précondition**:
```
g > 0
d > 0
0 ≤ p ≤ 1
```

**Variables**:
```
r: ROULETTE
l: BOOLEEN # Loop
```

**Initialisation**:
```
l <- Vrai
# Initialiser le générateur de la roulette
r.p <- p
initialiser(r)
```

**Realisation**
```
tant que l = Vrai
répéter
  suivant(r)

  si item(r) = 1
  alors
    # Droite
    si d = 0
    alors
      # On arrête de boucler.
      l = Faux
    sinon
      d <- d - 1
    fin si
  sinon
    # Gauche
    si g = 0
    alors
      # On arrête de boucler.
      l = Faux
    sinon
      g <- g - 1
    fin si
  fin si
fin répéter

si g = 0
alors
  Resultat <- d
sinon
  Resultat <- g
fin si
```

**Postcondition**:
```
Resultat = Nombre d'allumettes restantes dans l'autre boite.
```

Fin **allumer**

# Moyenne

Algorithme: **moyenne**

**Entrée**:
```
t: ENTIER # Nombre de tours pour les tests
g: ENTIER
d: ENTIER
p: REEL # Probabilité d'avoir droite. Ambidextre = 0.5. Un droitier tendra vers 1. Un gaucher tendra vers 0.
```

**Résultat**: REEL

**Précondition**:
```
t > 0
g > 0
d > 0
0 ≤ p ≤ 1
```

**Variables**:
```
i: ENTIER
somme: ENTIER
```

**Initialisation**:
```
i <- 0
somme <- 0
```

**Realisation**
```
tant que i < t
repeter
  somme <- somme + allumer(g, d, p)
  i <- i + 1
fin repeter

Resultat <- somme / t
```

**Postcondition**:
```
Resultat = Nombre moyen d'allumettes restantes dans l'autre boite.
```

Fin **moyenne**
