import { Roulette, suivant, item } from './roulette.ts';

function allumer(g: number, d: number, p: number): number {
  if (g <= 0 || d <= 0 || !(p >= 0 && p <= 1)) {
    throw new Error('Paramètres invalides.');
  }

  let l = true;
  let result: number;

  // Initialisation roulette.
  const roulette = new Roulette({ probability: p });

  while (l) {
    suivant(roulette);
    const droite = item(roulette);

    if (droite) {
      // Droite.
      if (d === 0) {
        l = false;
      } else {
        d--;
      }
    } else {
      // Gauche
      if (g === 0) {
        l = false;
      } else {
        g--;
      }
    }
  }

  if (g === 0) {
    result = d;
  } else {
    result = g;
  }

  return result;
}

function moyenne(tours: number, g: number, d: number, p: number): number {
  if (tours <= 0 || g <= 0 || d <= 0 || !(p >= 0 && p <= 1)) {
    throw new Error('Paramètres invalides.');
  }

  let somme = 0;
  for (let i = 0; i < tours; i++) {
    somme += allumer(g, d, p);
  }

  return somme / tours;
}

// Test de l'algorithme.
const AMBIDEXTRE = 0.5;
const DROITIER = 0.75;
const GAUCHER = 0.25;

console.log(`Moyenne d'un ambidextre: ${moyenne(100, 10, 10, AMBIDEXTRE)}`);
console.log(`Moyenne d'un droitier: ${moyenne(100, 10, 10, DROITIER)}`);
console.log(`Moyenne d'un gaucher: ${moyenne(100, 10, 10, GAUCHER)}`);
