export class Roulette {

  private probability: number;
  
  private random: number | undefined;

  constructor(init: { probability: number }) {
    this.probability = init.probability;
  }

  set p(value: number) {
    this.probability = value;
  }

  suivant(): void {
    this.random = Math.random();    
  }

  item(): boolean {
    if (this.random === undefined) {
      throw new Error('Appeler "suivant" avant d\'accéder à la valeur');
    }

    if (this.probability === undefined) {
      throw new Error('Probabilité non définie.');
    }

    return this.probability >= this.random;
  }

}


export function suivant(r: Roulette) {
  return r.suivant();
}

export function item(r: Roulette): boolean {
  return r.item();
}
